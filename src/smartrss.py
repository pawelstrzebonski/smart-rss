"""
Smart-RSS

A Python RSS feed manager that uses machine learning to
prioritize your feeds' links for you.
"""
import backend
import classifier
import frontend


def main():
    try:
        # Load the backend (database and feed handling)
        Backend = backend.Backend()
        # Load the text classifier (uses database for training and feed ordering)
        Classifier = classifier.Classifier(Backend)
        # Load the frontend (uses both backend and classifier)
        Frontend = frontend.Frontend(Backend, Classifier)
        # Start the main window
        Frontend.main_window()
    finally:
        # Close down the backend's database
        Backend.close()
