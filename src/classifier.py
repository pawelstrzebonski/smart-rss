"""
Text classifier for Smart-RSS, for predicting a
user's probability of opening/liking/disliking a
RSS link.
"""
from textblob import classifiers


class Classifier():
    """Text classifier class."""
    classifier_opened = None
    classifier_rating = None
    backend = None

    def __init__(self, backend):
        """Create the classifier provided the backend with database."""
        self.backend = backend
        cmd = """SELECT title, summary, tags, opened FROM links WHERE viewed=TRUE"""
        res = self.backend.cursor.execute(cmd)
        dataset = [(" ".join(x[:-1]), x[-1]) for x in res]
        self.classifier_opened = classifiers.NaiveBayesClassifier(dataset)
        cmd = """SELECT title, summary, tags, rating FROM links WHERE viewed=TRUE AND rating<>0"""
        res = self.backend.cursor.execute(cmd)
        dataset = [(" ".join(x[:-1]), x[-1]) for x in res]
        self.classifier_rating = classifiers.NaiveBayesClassifier(dataset)

    def partial_update(self, urls):
        """Update the classifiers (retrain)."""
        if len(urls) == 0:
            return
        links = "(" + ",".join("\"" + u['url'] + "\"" for u in urls) + ")"
        cmd = f"""SELECT title, summary, tags, opened FROM links WHERE viewed=TRUE AND url IN {links}"""
        res = self.backend.cursor.execute(cmd)
        dataset = [(" ".join(x[:-1]), x[-1]) for x in res]
        if len(dataset) > 0:
            self.classifier_opened.update(dataset)
        cmd = f"""SELECT title, summary, tags, rating FROM links WHERE viewed=TRUE AND rating<>0 AND url IN {links}"""
        res = self.backend.cursor.execute(cmd)
        dataset = [(" ".join(x[:-1]), x[-1]) for x in res]
        if len(dataset) > 0:
            self.classifier_rating.update(dataset)

    def predict_opened(self, link):
        """Return probability of opening the given link."""
        try:
            pred = self.classifier_opened.prob_classify(" ".join(
                [link['title'], link['summary'], link['tags']]))
            out = pred.prob(True)
        except:
            out = 0
        return out

    def predict_rating(self, link):
        """Return difference in probability of liking vs disliking the given link."""
        try:
            pred = self.classifier_rating.prob_classify(" ".join(
                [link['title'], link['summary'], link['tags']]))
            # Any better way of scoring?
            out = pred.prob(1) - pred.prob(-1)
        except:
            out = 0
        return out
