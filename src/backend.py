"""
Backend for Smart-RSS

Use a SQLite server to manage the RSS feeds and
their links, and feedparser to download and parse
the feeds.
"""

import sqlite3
import time
import datetime
from io import StringIO
from html.parser import HTMLParser
import logging
import feedparser as fp

# Setup logging to file
logging.basicConfig(filename='debug.log', level=logging.DEBUG)


# Taken from https://stackoverflow.com/a/925630
class MLStripper(HTMLParser):
    """HTML stripper."""
    def __init__(self):
        """Create the stripper."""
        super().__init__()
        self.reset()
        self.strict = False
        self.connectionvert_charrefs = True
        self.text = StringIO()

    def handle_data(self, data):
        """Feed the stripper."""
        self.text.write(data)

    def get_data(self):
        """Get string out of the stripper."""
        return self.text.getvalue()


def strip_tags(html):
    """Try to strip HTML tags from a string."""
    stripper = MLStripper()
    stripper.feed(html)
    return stripper.get_data()


def to_ascii(string):
    """Convert string into ASCII."""
    return string.encode("ascii", errors="ignore").decode()


class Backend():
    """Smart-RSS database and feed loading/parsing backend."""
    connection = None
    cursor = None

    def __init__(self, dbfile="smartrss.db"):
        """Setup the backend with a database at the given location/filename."""
        self.connection = sqlite3.connect(dbfile)
        self.cursor = self.connection.cursor()
        # Create table if it does not already exist
        self.cursor.execute("""CREATE TABLE IF NOT EXISTS feeds
        (url VARCHAR UNIQUE NOT NULL PRIMARY KEY,
        title VARCHAR NOT NULL,
        lastupdate TIMESTAMP NOT NULL,
        updateperiodseconds DATE)""")
        self.cursor.execute("""CREATE TABLE IF NOT EXISTS links
        (url VARCHAR NOT NULL,
        lastupdate DATETIME NOT NULL,
        title VARCHAR NOT NULL,
        summary VARCHAR NOT NULL,
        tags VARCHAR NOT NULL,
        viewed BOOL NOT NULL,
        opened BOOL NOT NULL,
        rating INT NOT NULL)""")

    def close(self):
        """Commit and close down the database."""
        # Save (commit) the changes
        self.connection.commit()
        # We can also close the connection if we are done with it.
        # Just be sure any changes have been committed or they will be lost.
        self.connection.close()

    def add_feed(self, url, updateperiodseconds=60 * 60):
        """Load the given RSS feed url and insert into the database."""
        try:
            feed = fp.parse(url)
            title = feed['feed']['title']
            lastupdate = datetime.datetime.now()
            # Check if already added previously
            cmd = """SELECT COUNT(*) FROM feeds WHERE url=?"""
            data = (url, )
            res = list(self.cursor.execute(cmd, data))
            if res[0][0] > 0:
                # If already exists, return without re-adding
                return True
            cmd = """INSERT INTO feeds (url, title, lastupdate, updateperiodseconds)
            VALUES (?,?,?,?)"""
            data = (url, title, lastupdate, updateperiodseconds)
            self.cursor.execute(cmd, data)
            self.update_feed(url)
            logging.info("Added feed: %s", url)
            return True
        except:
            logging.error("Error adding feed: %s", url)
            return False

    def add_link(self, link):
        """Prepare a given link from feedparser and insert into the database."""
        try:
            title = to_ascii(link['title'])
            summary = to_ascii(strip_tags(link['summary']))
            url = link['link']
            tags = " ".join([to_ascii(t['term']) for t in link['tags']])
            date = time.asctime(link['published_parsed'])
            # Check if already added previously
            cmd = """SELECT COUNT(*) FROM links WHERE url=?"""
            data = (url, )
            res = list(self.cursor.execute(cmd, data))
            if res[0][0] > 0:
                # If already exists, return without re-adding
                return True
            cmd = """INSERT INTO links
    (url, lastupdate, title, summary, tags, viewed, opened, rating)
    VALUES (?,?,?,?,?,?,?,?)"""
            data = (url, date, title, summary, tags, False, False, 0)
            self.cursor.execute(cmd, data)
            logging.info("Added link: %s", link['link'])
            return True
        except:
            logging.error("Adding link: %s", link['link'])
            return False

    def get_unread_links(self):
        """Return a list of link dicts for all links marked as
not-yet-viewed in the database."""
        res = self.cursor.execute(
            "SELECT url, title, summary, tags FROM links WHERE NOT viewed")
        res = [{
            "title": x[1],
            "summary": x[2],
            "url": x[0],
            "tags": x[3]
        } for x in res]
        logging.info("Got unread links")
        return res

    def mark_viewed(self, url):
        """Mark the specified link as seen in the database."""
        try:
            cmd = """UPDATE links SET viewed=TRUE WHERE url = ?"""
            data = (url, )
            self.cursor.execute(cmd, data)
            logging.info("Marked read: %s", url)
            return True
        except:
            logging.error("Marking read: %s", url)
            return False

    def like_link(self, url):
        """Mark the specified link as liked in the database."""
        try:
            cmd = """UPDATE links SET rating=1 WHERE url = ?"""
            data = (url, )
            self.cursor.execute(cmd, data)
            logging.info("Liked link: %s", url)
            return True
        except:
            logging.error("Liking link: %s", url)
            return False

    def dislike_link(self, url):
        """Mark the specified link as dis-liked in the database."""
        try:
            cmd = """UPDATE links SET rating=-1 WHERE url = ?"""
            data = (url, )
            self.cursor.execute(cmd, data)
            logging.info("Disliked link: %s", url)
            return True
        except:
            logging.error("Disliking link: %s", url)
            return False

    def mark_opened_link(self, url):
        """Mark the specified link as opened in the database."""
        try:
            cmd = """UPDATE links SET opened=TRUE WHERE url = ?"""
            data = (url, )
            self.cursor.execute(cmd, data)
            logging.info("Marked opened: %s", url)
            return True
        except:
            logging.error("Marking opened: %s", url)
            return False

    def update_feed(self, url):
        """Update the feed with the provided url and add its links to the database."""
        feed = fp.parse(url)
        for link in feed['entries']:
            try:
                self.add_link(link)
            except:
                pass
        cmd = """UPDATE feeds SET lastupdate = DATETIME('NOW') WHERE url = ?"""
        data = (url, )
        self.cursor.execute(cmd, data)
        logging.info("Updated feed: %s", url)

    def update_feeds(self):
        """Update all feeds that have not been updated too recently."""
        try:
            cmd = """SELECT url FROM feeds
    WHERE strftime('%s','now')-strftime('%s',lastupdate)>updateperiodseconds"""
            data = ()
            results = self.cursor.execute(cmd, data)
            for result in results:
                url = result[0]
                try:
                    self.update_feed(url)
                    logging.info("Updated %s", url)
                except:
                    logging.error("Updating: %s", url)
            logging.info("Updated all feeds")
            return True
        except:
            logging.error("Error during updating all feed")
            return False
