"""
GUI frontend to the Smart-RSS application.
"""

import webbrowser
import PySimpleGUI as sg


def open_url(url):
    """Open given url in browser."""
    webbrowser.open_new(url)


def make_link(i):
    """Create GUI element for a RSS feed's link item."""
    return [
        sg.Text(key=f'-TITLE{i}-',
                background_color='black',
                text_color='white',
                size=(30, 2)),
        sg.Output(key=f'-SUMMARY{i}-', size=(60, 2)),
        sg.Text(key=f'-SCORE{i}-',
                background_color='black',
                text_color='white',
                size=(5, 1)),
        sg.Button("Open", button_color=('white', 'gray'),
                  key="Open " + str(i)),
        sg.Button("Like",
                  button_color=('white', 'green'),
                  key="Like " + str(i)),
        sg.Button("Dislike",
                  button_color=('white', 'red'),
                  key="Dislike " + str(i)),
    ]


class Frontend():
    """GUI frontend for Smart-RSS."""
    backend = None
    classifier = None
    linksperpage = 5

    def __init__(self, backend, classifier):
        """Create the frontend connected up to the provided database backend and classifier."""
        self.classifier = classifier
        self.backend = backend

    def add_feed(self, url, updatetime):
        """Add given url as RSS feed to database."""
        try:
            self.backend.add_feed(url, updatetime)
            return True
        except:
            return False

    def add_feed_window(self):
        """Open window for adding a new feed."""
        # All the stuff inside your window.
        head = [[sg.Text('Feed URL:'), sg.InputText(key="-URL-")]]
        body = [[
            sg.Text('Minimal time between updates (seconds):'),
            sg.InputText('3600', key="-TIME-")
        ]]
        foot = [[sg.Button("Add Feed"), sg.Button('Cancel')]]
        layout = head + body + foot

        # Create the Window
        window = sg.Window('Smart-RSS: Add Feed', layout)
        # Event Loop to process "events" and get the "values" of the inputs
        while True:
            event, values = window.read()
            if event in (sg.WIN_CLOSED, 'Cancel'):
                break
            elif event == "Add Feed":
                try:
                    updatetime = int(values["-TIME-"])
                    print(updatetime)
                    if self.add_feed(values["-URL-"], updatetime):
                        break
                except:
                    pass
        window.close()

    def sort_links(self, links, sortby='rating'):
        """Sort feeds' links by likelihood of opening or (dis)liking."""
        if sortby == 'opened':
            sortfun = self.classifier.predict_opened
        elif sortby == 'rating':
            sortfun = self.classifier.predict_rating
        else:
            sortfun = lambda l: 1
        for link in links:
            link['score'] = sortfun(link)
        # Sort links in decreasing order of probability of opening or positive rating
        links.sort(key=lambda link: link['score'], reverse=True)
        return links

    def update_links(self, window, links):
        """Update the link GUI elements in the window with the provided feed links."""
        for i in range(self.linksperpage):
            if i < len(links):
                window[f'-SUMMARY{i}-'].update(links[i]['summary'])
                window[f'-TITLE{i}-'].update(links[i]['title'])
                window[f'-SCORE{i}-'].update(str(round(links[i]['score'], 3)))
            else:
                window[f'-SUMMARY{i}-'].update('')
                window[f'-TITLE{i}-'].update('')
                window[f'-SCORE{i}-'].update('')

    def main_window(self):
        """Open main window of GUI."""
        links = self.sort_links(self.backend.get_unread_links())
        # All the stuff inside your window.
        head = [[sg.Button("Update Feeds"), sg.Button("Next Feeds")]]
        foot = [[sg.Button("Add Feed"), sg.Button('Exit')]]
        body = [[make_link(l)] for l in range(self.linksperpage)]
        layout = head + body + foot

        # Create the Window
        window = sg.Window('Smart-RSS', layout, finalize=True)
        self.update_links(window, links)

        # Event Loop to process "events" and get the "values" of the inputs
        while True:
            event, _ = window.read()
            if event in (sg.WIN_CLOSED,
                         'Exit'):  # if user closes window or clicks cancel
                break
            elif event == "Add Feed":
                self.add_feed_window()
            elif "Like" in event:
                i = int(event.split()[1])
                if i < len(links):
                    url = links[i]['url']
                    self.backend.like_link(url)
            elif "Dislike" in event:
                i = int(event.split()[1])
                if i < len(links):
                    url = links[i]['url']
                    self.backend.dislike_link(url)
            elif "Open" in event:
                i = int(event.split()[1])
                if i < len(links):
                    url = links[i]['url']
                    open_url(url)
                    self.backend.mark_viewed(url)
                    self.backend.mark_opened_link(url)
            elif event == "Update Feeds":
                self.backend.update_feeds()
            elif event == "Next Feeds":
                seenlinks = []
                for link in links[:min(self.linksperpage, len(links))]:
                    seenlinks.append(link)
                    self.backend.mark_viewed(link['url'])
                self.classifier.partial_update(seenlinks)
                links = self.sort_links(self.backend.get_unread_links())
            self.update_links(window, links)
        # Finally, close the window
        window.close()
