# TODOs

This project is by no means finished/polished. A the following is a shopping list of improvements (in no particular order):

* Better/faster GUI (expose more application settings, move away from PySimpleGUI?)
* Automatic feed updating (on application startup, on a timer, etc)
* Better handling of HTML/non-ASCII content (work with it rather than strip it out)
* Database cleanup (remove old entries periodically, options to remove feeds)
* General code/project improvements (tests, packaging, etc)
