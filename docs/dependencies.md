# Dependencies

This application makes use of the following Python packages:

* [`sqlite3`](https://docs.python.org/3/library/sqlite3.html): SQLite database for storing feed information
* [`feedparser`](https://github.com/kurtmckee/feedparser): Fetch and parse RSS feeds
* [`time`](https://docs.python.org/3/library/time.html)/[`datetime`](https://docs.python.org/3/library/datetime.html): Keeping track of when the feeds were last updated
* [`webbrowser`](https://docs.python.org/3/library/webbrowser.html): Opening links from this applications in the user's default browser
* [`io`](https://docs.python.org/3/library/io.html)/[`html`](https://docs.python.org/3/library/html.html): Stripping HTML content from feed's titles/summaries
* [`textblob`](https://github.com/sloria/textblob): Implements classification/rating of feed links based on their textual features
* [`PySimpleGUI`](https://github.com/PySimpleGUI/PySimpleGUI/): GUI interface
* [`logging`](https://docs.python.org/3/library/logging.html): Logging of errors/events in the backend for debugging purposes
