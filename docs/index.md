# Smart-RSS

Smart-RSS is a machine learning enhanced RSS feed manager, written in Python.

This is by no means a complete and polished product. Rather it is a pet project to practice and learn SQL/databases and GUI.

For a Rust or Go re-implementation, see [(Rusty) Smart-RSS](https://gitlab.com/pawelstrzebonski/rusty-smart-rss) or [Smart-RSS Go](https://gitlab.com/pawelstrzebonski/smart-rss-go).

## Screenshots

![Add-a-new-RSS-feed window](smartrsspy_addfeed.png)

Window for adding a new RSS feed

![Main window](smartrsspy_main.png)

Main window with scored feed items
