# Design Document

## Files

This application is broken into three primary files:

* `backend.py`
* `classifier.py`
* `frontend.py`

The `backend.py` file proves the `Backend` class. This handles the SQLite server that manages the RSS feeds and the items that those feed have served to the user. Fetching and parsing of the RSS feeds is acomplished using the `feedparser` package.

The `classifier.py` file provides the `Classifier` class which uses `textblob` for predicting whether or not a user will open a link from an RSS feed, and whether the will "like" or "dislike" it, where (dis)liking a link is a local in-app process.

The `frontend.py` file provides the `Frontend` class that pulls together the `Backend` and `Classifier` and provides a GUI to the user using the `PySimpleGUI` package.

## Backend

The `feedparser` package is used to download and parse RSS feed and the items they server. For simplicity, this application will try to strip any/all HTML and non-ASCII content from recieved content.

The `sqlite3` package is used to store the feeds and the items retrieved from them. These are stored in the `smartrss.db` database file, which will be created if it does not already exist. The database contains two tables:

* "feeds"
* "links"

The "feeds" table stores information regarding the feed sources. In particular, for each RSS source is stores:

* "url": The URL of the RSS feed
* "title": The title of the RSS feed
* "lastupdate": The date/time of the last time the feed was updated from its source
* "updateperiodseconds": The minimal duration (in seconds) between updates of this source

This table is largely static, except when the user adds a new feed and when a feed is updated and the date/time of updating is refreshed.

The "links" table store the main information of the links obtained from the feed sources. In particular, for each link served by an RSS source it stores:

* "url": The URL of the link
* "lastupdate": The date/time of link publishing (as per the RSS feed's data)
* "title": The title of the link
* "summary": The summary text of the link (stripped of formatting and non-ASCII content)
* "tags": A string of the feed-provided tags pertinent to the link
* "viewed": A boolean indicating whether or not this link has been shown to the user in this application
* "opened": A boolean indicating whether or not the user has opened this link via this application
* "rating": An integer indicating whether the user has "liked" this link (+1), "disliked" it (-1), or neither (0)

## Classifier

The `textblob` package is used to estimate the probability that will either click on a link or (dis)like within this application. As of writing, it uses a Naive Bayes classifier that is trained on links in the `Backend`'s database that are marked as been already shown to the user (and have been either "liked" or "disliked" for the predictor of "rating"). These predictions are based on a combination of the textual content of the link's "title", "summary" and "tags".

## Frontend

The `PySimpleGUI` package is used to enable user interactivity via a GUI. As of writing, there are two windows that may be shown to the user.

The main window show by default. It displays (by default) 5 of the links from the database with their title, summary text, and a predicted score value (obtained using the `Classifier`, may be related to the probability of clicking on the link or likelihood of (dis)liking it). The links will be displayed in (predicted) order of desirability/interest to the user. Each link's GUI element will have a button to open the full article in the user's default browser, as well as a pair of buttons to mark the link as either "liked" or "disliked" within this application.

The main window also provides buttons to update the feeds (new links will be fetched from each source, provided enough time has lapsed since the last update), add a new RSS feed source (opens a new window), show the next set of new links (in the process removing the currently displayed links and marking them as viewed in the `Backend`'s database), as well as a button to exit the application.

The second window is the window for adding a new RSS feed source, which simply asks for the URL of the feed source and the minimal time between feed updates (in seconds, by default 3600, i.e. 1 hour).
