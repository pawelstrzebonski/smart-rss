# Smart-RSS

A machine learning enhanced RSS feed manager, written in Python. For the subsequent Rust or Go reimplementations, see [(Rusty) Smart-RSS](https://gitlab.com/pawelstrzebonski/rusty-smart-rss) and [Smart-RSS Go](https://gitlab.com/pawelstrzebonski/smart-rss-go).

Note: This is more of a learning side-project than a polished and ready-to-use program.

For more information about program design/implementation, please consult the [documentation pages](https://pawelstrzebonski.gitlab.io/smart-rss/).

## Usage/Installation

The Python code is in the `src/` directory. The `src/requirements.txt` file lists the (non-standard library) packages that are dependencies of this project.

Note: The `textblob` package used in the project requires some additional setup. Refer to the [`textblob` project](https://github.com/sloria/textblob#get-it-now) for setup instructions.
